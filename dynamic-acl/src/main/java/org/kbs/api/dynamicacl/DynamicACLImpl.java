package org.kbs.api.dynamicacl;
/**
 * Created by kcn on 14/12/1.
 */

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.thrift.TException;
import org.kbs.api.dynamicacl.thrift.DynamicACLService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicInteger;

@SuppressWarnings("UnusedDeclaration")
public class DynamicACLImpl implements DynamicACLService.Iface {
    private static final Logger LOG = LoggerFactory.getLogger(DynamicACLImpl.class);

    @Autowired
    private IAgedACLTable aclBlockTable;

    @Autowired
    private ACLTable aclWhiteTable;

    @Autowired
    private ACLTable aclBlackTable;

    @Autowired
    private ACLTable aclGrayTable;

    public IAgedACLTable getAclBlockTable() {
        return aclBlockTable;
    }

    public void setAclBlockTable(IAgedACLTable aclBlockTable) {
        this.aclBlockTable = aclBlockTable;
    }

    public ACLTable getAclWhiteTable() {
        return aclWhiteTable;
    }

    public void setAclWhiteTable(ACLTable aclWhiteTable) {
        this.aclWhiteTable = aclWhiteTable;
    }

    public ACLTable getAclBlackTable() {
        return aclBlackTable;
    }

    public void setAclBlackTable(ACLTable aclBlackTable) {
        this.aclBlackTable = aclBlackTable;
    }

    public ACLTable getAclGrayTable() { return aclGrayTable; }

    public void setAclGrayTable(ACLTable aclGrayTable) { this.aclGrayTable = aclGrayTable; }

    private long expireTime=2*60*60*24*1000; //缺省记录2天

    /**
     * 获得毫秒单位的超时时间
     * @return 超时时间
     */
    public long getExpireTime() {
        return expireTime;
    }

    /**
     * 设置毫秒单位的超时时间
     * @param expireTime
     */
    public void setExpireTime(long expireTime) {
        this.expireTime = expireTime;
    }

    //ip long  to String
    public String iplongToIp(long ipaddress) {
        StringBuffer sb = new StringBuffer("");
        sb.append(String.valueOf((ipaddress >>> 24)& 0xFF));
        sb.append(".");
        sb.append(String.valueOf((ipaddress & 0x00FFFFFF) >>> 16 & 0xFF));
        sb.append(".");
        sb.append(String.valueOf((ipaddress & 0x0000FFFF) >>> 8 & 0xFF));
        sb.append(".");
        sb.append(String.valueOf((ipaddress & 0x000000FF)));
        return sb.toString();
    }

    private ConcurrentLinkedDeque<Pair<Integer,Long>> logQueue= new ConcurrentLinkedDeque<>();
    private ErrorCount[] errorCounts={
            new ErrorCount(this,1*24*60*60*1000), //一天
            new ErrorCount(this,2*60*60*1000),    //两小时
            new ErrorCount(this,1*60*60*1000)     //一小时
                        };

    public AtomicInteger logCount=new AtomicInteger(0);

    public int getLogCount() {
        return logCount.get();
    }

    @Override
    public int dynamic_acl_add_record(String id, int ip) throws TException {
        LOG.debug("add {} from {}", id, iplongToIp(ip));
        logQueue.add(new ImmutablePair<>(ip,System.currentTimeMillis()));
        logCount.incrementAndGet();

        int[] counts=new int[errorCounts.length];
        for (int i=0;i<errorCounts.length;i++)
            counts[i]=errorCounts[i].inc(ip);
	    /* 如果一天内累计错误100次，封禁半天；若错误次数超过20 或 过去2小时内错误次数超过15，封禁1个小时；1小时超过10次封禁10分钟 */
        String ipstr=iplongToIp(ip);
        SubnetUtils2 subnet=new SubnetUtils2(ipstr,"255.255.255.255");
        int rate=1;
        if (aclGrayTable.isInTable(ip)!=0)
            rate=3;
        if (counts[0]>=100*rate) {
            aclBlockTable.addSubnet(subnet, 12 * 60 * 60 * 1000);
        } else if (counts[0]>=20*rate) {
            aclBlockTable.addSubnet(subnet, 1 * 60 * 60 * 1000);
        }
        if (counts[1]>=15*rate) {
            aclBlockTable.addSubnet(subnet, 1 * 60 * 60 * 1000);
        }
        if (counts[2]>=10*rate) {
            aclBlockTable.addSubnet(subnet, 10 * 60 * 1000);
        }
        return 0;
    }

    @Override
    public int dynamic_acl_clear() throws TException {
        LOG.info("clean ACL table");
        aclBlockTable.clear();
        return 0;
    }

    /**
     * 检查IP是否被封禁
     * @param   ip: IP地址
     * @return   0: 正常IP未被封禁
     * @return  >0: 已被封禁, 返回的值为距离解除封禁的时长(s)
     * @return  <0: dynamic ACL的黑名单
     */

    @Override
    public int dynamic_acl_check_ip(int ip) throws TException {

        LOG.debug("check ip {}",iplongToIp(ip));
        if (aclWhiteTable.isInTable(ip)>0) {
            return 0;
        }
        if (aclBlackTable.isInTable(ip)>0) {
            return -1;
        }
        return aclBlockTable.isInTable(ip);
    }

    public void doExpire() {
        for (int i=0;i<errorCounts.length;i++)
            errorCounts[i].doExpire();
        long time=System.currentTimeMillis();
        int count=0;
        while (true) {
            Pair<Integer,Long> log=logQueue.peek();
            if (log==null)
                break;
            if ((log.getRight()+expireTime)>=time)
                break;
            logQueue.poll();
            count++;
            logCount.decrementAndGet();
        }
        if (count>0)
            LOG.info("remove {} error log",count);
        aclBlockTable.doExpire();
    }

    private class ErrorCount {
        private ConcurrentHashMap<Integer,Integer> errorCount=new ConcurrentHashMap<>();
        private Iterator<Pair<Integer,Long>> lastCheck;
        private long countPeriod;
        private Pair<Integer,Long> lastCheckELement;
        private DynamicACLImpl parent;

        public ErrorCount(DynamicACLImpl parent,long countPeriod) {
            this.parent=parent;
            this.countPeriod=countPeriod;
            this.lastCheck=null;
            this.lastCheckELement=null;
        }

        public int getCount(int ip) {
            return errorCount.get(ip);
        }

        public int inc(int ip) {
            int count;
            synchronized (this) {
                if (errorCount.containsKey(ip)) {
                        count=errorCount.get(ip)+1;
                        errorCount.put(ip,count);
                } else {
                        errorCount.put(ip,1);
                        count=1;
                }
            }
            return count;
        }

        public int dec(int ip) {
            int count;
            synchronized (this) {
                if (errorCount.containsKey(ip)) {
                        count=errorCount.get(ip)-1;
                        if (count!=0)
                            errorCount.put(ip,count);
                        else
                            errorCount.remove(ip);
                } else
                    count=0;
            }
            return count;
        }

        public void doExpire() {
            synchronized (this) {
                if (logQueue.isEmpty())
                    return;
                if (lastCheckELement==null) {
                    if (lastCheck==null) {
                        lastCheck=logQueue.descendingIterator();
                    }
                    if (lastCheck.hasNext())
                        lastCheckELement=this.lastCheck.next();
                    else //没有元素
                        return;
                }
                long currentTime=System.currentTimeMillis();
                while (lastCheckELement.getValue()+countPeriod<=currentTime) {
                    //超时处理
                    dec(lastCheckELement.getKey());
                    //需要移动指针到下一个
                    if (lastCheck.hasNext())
                        lastCheckELement=lastCheck.next();
                    else {
                        //如果没有则结束
                        lastCheckELement=null;
                        break;
                    }
                }
            }
        }
    }
}
