package org.kbs.api.dynamicacl;
/**
 * Created by kcn on 14/12/5.
 */

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import org.apache.commons.cli.*;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TThreadedSelectorServer;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TNonblockingServerTransport;
import org.apache.thrift.transport.TTransportException;
import org.kbs.api.dynamicacl.thrift.DynamicACLService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.net.InetSocketAddress;

@SuppressWarnings("UnusedDeclaration")
public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);
    public static void main(String[] args) throws TTransportException, IOException {

        Options options = new Options();
        options.addOption("h", "help", false, "print help for the command.");
        options.addOption("b", "bind", true, "Server Address.");
        options.addOption("p", "port", true, "Server Port");
        options.addOption(null, "whitetable", true, "White Subnet Table");
        options.addOption(null, "blacktable", true, "Black Subnet Table");
        options.addOption(null, "graytable", true, "Gray Subnet Table");
        options.addOption("l", "logback", true, "Logback configuation file. eg:./logback.xml");

        String formatstr = "ApiServer [-h/--help][-b bind_ip][-p bind_port]";

        HelpFormatter formatter = new HelpFormatter();
        CommandLineParser parser = new PosixParser();
        CommandLine cl = null;
        try {
            // 处理Options和参数
            cl = parser.parse(options, args);
        } catch (ParseException e) {
            formatter.printHelp(formatstr, options); // 如果发生异常，则打印出帮助信息
            return;
        }

        if (cl.hasOption('h')) {
            formatter.printHelp(formatstr, options);
            return;
        }

        int port = 7911;
        String hostname = "127.0.0.1";
        if (cl.hasOption('b')) {
            hostname = cl.getOptionValue('b');
        }
        if (cl.hasOption('l')) {
            changeLogbackConfig(cl.getOptionValue('l'));
        }
        if (cl.hasOption('p')) {
            port = Integer.parseInt(cl.getOptionValue('p'));
        }

        try (ConfigurableApplicationContext context =
                     new ClassPathXmlApplicationContext("classpath:spring.xml")) {
            context.registerShutdownHook();

            if (cl.hasOption("graytable")) {
                String graytable=cl.getOptionValue("graytable");
                ACLTable aclGrayTable=(ACLTable)context.getBean("aclGrayTable");
                aclGrayTable.loadList(graytable);
            }

            if (cl.hasOption("whitetable")) {
                String table=cl.getOptionValue("whitetable");
                ACLTable aclTable=(ACLTable)context.getBean("aclWhiteTable");
                aclTable.loadList(table);
            }

            if (cl.hasOption("blacktable")) {
                String table=cl.getOptionValue("blacktable");
                ACLTable aclTable=(ACLTable)context.getBean("aclBlackTable");
                aclTable.loadList(table);
            }

            DynamicACLServer server=context.getBean(DynamicACLServer.class);
            server.setHostname(hostname);
            server.setPort(port);
            server.start();
        } catch (final Exception e) {
            // handle exceptions properly here
            e.printStackTrace();
        }
    }

    private static void changeLogbackConfig(String logbackxml) {
        try {
            LOG.info("Set logback configuration:{}",logbackxml);
            LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
            JoranConfigurator configurator = new JoranConfigurator();
            configurator.setContext(context);
            // Call context.reset() to clear any previous configuration, e.g. default
            // configuration. For multi-step configuration, omit calling context.reset().
            context.reset();
            configurator.doConfigure(logbackxml);
        } catch (JoranException je) {
            // StatusPrinter will handle this
            je.printStackTrace();
        }
    }

}
