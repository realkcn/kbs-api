package org.kbs.api.dynamicacl;
/**
 * Created by kcn on 14/12/1.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class AgedACLTableMapImpl implements Externalizable, IAgedACLTable {
    private static final Logger LOG = LoggerFactory
            .getLogger(AgedACLTableMapImpl.class);

    private static final long serialVersionUID = -8025678716491637206L;

    private ConcurrentHashMap<SubnetUtils2,Long> subnets;

    private int defaultExpiredTime;

    private String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public AgedACLTableMapImpl() {
    }

    private AgedACLTableMapImpl(final String name, final int defaultExpiredTime) {
        init(name,defaultExpiredTime);
    }

    public static AgedACLTableMapImpl createAgedACLTable(final String name,final int defaultExpiredTime) {
        return new AgedACLTableMapImpl(name,defaultExpiredTime);
    }

    /**
     * 初始化ACL表
     */
    @Override
    public void init(final String name, final int defaultExpiredTime) {
        this.name=name;
        this.defaultExpiredTime=defaultExpiredTime;
        subnets=new ConcurrentHashMap<>();
    }

    /**
     * 清空ACL表
     */
    @Override
    public void clear() {
        LOG.debug("清空acl表");
        init(getName(),defaultExpiredTime);
    }

    /**
     * 增加一个子网，如果已经存在则更新封禁时间，取最长的
     * @param subnetstr 子网
     * @param expiredTime 超时时间，毫秒
     */
    @Override
    public void addSubnet(String subnetstr, long expiredTime) {
        SubnetUtils2 subnet=new SubnetUtils2(subnetstr);
        addSubnet(subnet,expiredTime);
    }

    /**
     * 增加一个子网，如果已经存在则更新封禁时间，取最长的
     * @param subnet 子网
     */
    @Override
    public void addSubnet(SubnetUtils2 subnet, long expiredTime) {
        long banTime=expiredTime == 0 ? defaultExpiredTime : expiredTime;
        if (!isInTable(subnet)) {
            subnets.put(subnet, System.currentTimeMillis() +
                    banTime);
            LOG.info("Ban {} {} minutes.", subnet.getInfo().getCidrSignature(), banTime/60/1000);
        }
        else {
            long oldExpiredTime=subnets.get(subnet);
            long newExpiredTime=System.currentTimeMillis()+banTime;
            if (newExpiredTime>oldExpiredTime) {
                subnets.put(subnet, newExpiredTime);
                if (newExpiredTime>oldExpiredTime+60*10*1000) {//封禁时间超过10分钟就log
                    LOG.info("Ban {} {} minutes.", subnet.getInfo().getCidrSignature(), banTime/60/1000);
                }
            }
        }
    }

    /**
     * 删除一个子网
     * @param subnet 需要删除的子网
     */
    @Override
    public void removeSubnet(SubnetUtils2 subnet) {
        subnets.remove(subnet);
    }

    /**
     * 检查子网是否已经在表里
     * @param subnet 需检查的子网
     * @return true or flase
     */
    @Override
    public boolean isInTable(SubnetUtils2 subnet) {
        Long expiredTime=subnets.get(subnet);
        if (expiredTime==null)
            return false;
        if (System.currentTimeMillis()>expiredTime) {
            return false;
        }
        return true;
    }

    /**
     * 检查ip是否已经在表里
     * @param ipaddress 需检查的ip
     * @return 以秒为单位的超时时间
     */
    @Override
    public int isInTable(int ipaddress) {
        Iterator it=subnets.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry)it.next();
            long expiredTime= System.currentTimeMillis()-(Long) entry.getValue();
            if (expiredTime>=0)
                continue;
            SubnetUtils2 subnet= (SubnetUtils2) entry.getKey();
            if (subnet.getInfo().isInRange(ipaddress)) {
                int ret=(int) -expiredTime / 1000;
                return ret==0?1:ret;
            }
        }
        return 0;
    }

    @Override
    public void doExpire() {
//        LOG.info("Deal expired ip in {}", getName());
        Iterator it=subnets.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry)it.next();
            long expiredTime= (Long) entry.getValue();
            if (System.currentTimeMillis() < expiredTime)
                continue;
            LOG.info("remove {} in {}", ((SubnetUtils2)entry.getKey()).getInfo().getCidrSignature(),
                    getName());
            it.remove();
        }
    }

    /**
     * The object implements the writeExternal method to save its contents
     * by calling the methods of DataOutput for its primitive values or
     * calling the writeObject method of ObjectOutput for objects, strings,
     * and arrays.
     *
     * @param out the stream to write the object to
     * @throws java.io.IOException Includes any I/O exceptions that may occur
     * @serialData Overriding methods should use this tag to describe
     * the data layout of this Externalizable object.
     * List the sequence of element types and, if possible,
     * relate the element to a public/protected field and/or
     * method of this Externalizable class.
     */
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        Iterator it=subnets.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry)it.next();
            if (entry!=null) {
                SubnetUtils2 subnet = (SubnetUtils2) entry.getKey();
                out.writeObject(subnet.getInfo().getAddress());
                out.writeObject(subnet.getInfo().getNetmask());
                out.writeLong((Long)entry.getValue());
            } else {
                break;
            }
        }
        out.writeObject(null);
        out.writeObject(name);
        out.write(defaultExpiredTime);
    }

    /**
     * The object implements the readExternal method to restore its
     * contents by calling the methods of DataInput for primitive
     * types and readObject for objects, strings and arrays.  The
     * readExternal method must read the values in the same sequence
     * and with the same types as were written by writeExternal.
     *
     * @param in the stream to read data from in order to restore the object
     * @throws java.io.IOException    if I/O errors occur
     * @throws ClassNotFoundException If the class for an object being
     *                                restored cannot be found.
     */
    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        subnets=new ConcurrentHashMap<>();
        while (true) {
            String address,netmask;
            address=(String)in.readObject();
            if (address==null)
                break;
            netmask=(String)in.readObject();
            SubnetUtils2 subnet=new SubnetUtils2(address,netmask);
            Long expiredValue=in.readLong();
            subnets.put(subnet,expiredValue);
        }
        name=(String)in.readObject();
        defaultExpiredTime=in.readInt();
    }
}
