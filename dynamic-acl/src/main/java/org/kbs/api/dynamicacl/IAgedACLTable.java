package org.kbs.api.dynamicacl;/**
 * Created by kcn on 14/12/4.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface IAgedACLTable {
    String getName();

    void setName(String name);

    /**
     * 初始化ACL表
     */
    void init(String name, int defaultExpiredTime);

    /**
     * 清空ACL表
     */
    void clear();

    /**
     * 增加一个子网，如果已经存在则更新封禁时间，取最长的
     * @param subnetstr 子网
     * @param expiredTime 超时时间，毫秒
     */
    void addSubnet(String subnetstr, long expiredTime);

    /**
     * 增加一个子网，如果已经存在则更新封禁时间，取最长的
     * @param subnet 子网
     */
    void addSubnet(SubnetUtils2 subnet, long expiredTime);

    /**
     * 删除一个子网
     * @param subnet 需要删除的子网
     */
    void removeSubnet(SubnetUtils2 subnet);

    /**
     * 检查子网是否已经在表里
     * @param subnet 需检查的子网
     * @return true or flase
     */
    boolean isInTable(SubnetUtils2 subnet);

    /**
     * 检查ip是否已经在表里
     * @param ipaddress 需检查的ip
     * @return 以秒为单位的超时时间
     */
    int isInTable(int ipaddress);

    void doExpire();
}
