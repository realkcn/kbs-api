package org.kbs.api.dynamicacl;
/**
 * Created by kcn on 14/12/3.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SuppressWarnings("UnusedDeclaration")
public class IpAddressUtils {
    private static final Logger LOG = LoggerFactory.getLogger(IpAddressUtils.class);

    public static int ipToInt(String strIp) {
        try {
            byte[] ip= InetAddress.getByName(strIp).getAddress();
            return (ip[0] << 24 & 0xFF000000)
                    | (ip[1] << 16 & 0x00FF0000)
                    | (ip[2] << 8 & 0x0000FF00)
                    | ip[3];
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return 0;
        }
    }

}
