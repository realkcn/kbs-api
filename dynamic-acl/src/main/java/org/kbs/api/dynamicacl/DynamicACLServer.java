package org.kbs.api.dynamicacl;
/**
 * Created by kcn on 14/12/1.
 */

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import org.apache.commons.cli.*;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadedSelectorServer;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TNonblockingServerTransport;
import org.apache.thrift.transport.TTransportException;
import org.kbs.api.dynamicacl.thrift.DynamicACLService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import java.io.IOException;
import java.net.InetSocketAddress;

@SuppressWarnings("UnusedDeclaration")
@ManagedResource(objectName = "org.kbs.api.dynamicacl:name=ServerJmxBean", description = "Dynamic ACL Server")
public class DynamicACLServer {
    private static final Logger LOG = LoggerFactory.getLogger(DynamicACLServer.class);

    private TServer server;

    @Autowired
    DynamicACLImpl service;

    private String hostname;

    private int port;

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @ManagedOperation
    public void start() {
        LOG.info("Starting server...");
        try {
            InetSocketAddress bindaddress = new InetSocketAddress(hostname, port);

            TNonblockingServerTransport serverTransport = new TNonblockingServerSocket(bindaddress);
            DynamicACLService.Processor<DynamicACLImpl> processor = new DynamicACLService.Processor<>(service);
            server = new TThreadedSelectorServer(
                    new TThreadedSelectorServer.Args(serverTransport)
                            .inputProtocolFactory(new TBinaryProtocol.Factory())
                            .outputProtocolFactory(new TBinaryProtocol.Factory())
                            .processor(processor));
            LOG.info("Running server...");
            server.serve();
            LOG.info("Server shutdowned...");
        } catch (TTransportException e) {
            e.printStackTrace();
        }
    }

    @ManagedOperation
    public void stop() {
        server.stop();
        int count = 0;
        while (server.isServing()) {
            try {
                Thread.sleep(1 * 1000);
                count++;
                if (count >= 10) {//最多等待10秒
                    LOG.info("Waiting shutdown timeout");
                    break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
