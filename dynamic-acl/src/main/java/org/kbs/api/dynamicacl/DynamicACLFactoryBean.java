package org.kbs.api.dynamicacl;
/**
 * Created by kcn on 14/12/3.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

@SuppressWarnings("UnusedDeclaration")
public class DynamicACLFactoryBean implements FactoryBean<DynamicACLImpl> {
    private static final Logger LOG = LoggerFactory.getLogger(DynamicACLFactoryBean.class);

    private int deserializationCount;

    private Resource source;

    public DynamicACLFactoryBean() {
    }

    public DynamicACLFactoryBean(Resource source) {
        this.source = source;
    }

    public Resource getSource() {
        return source;
    }

    public void setSource(Resource source) {
        this.source = source;
    }

    @Override
    public DynamicACLImpl getObject() throws Exception {
        try (ObjectInputStream oin = new ObjectInputStream(new BufferedInputStream(source.getInputStream()))) {
            DynamicACLImpl result = (DynamicACLImpl)oin.readObject();
            return result;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new DynamicACLImpl();
    }

    @Override
    public Class<?> getObjectType() {
        return DynamicACLImpl.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
