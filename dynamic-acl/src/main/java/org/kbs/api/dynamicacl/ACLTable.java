package org.kbs.api.dynamicacl;
/**
 * Created by kcn on 14/12/1.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import java.io.*;
import java.util.*;

public class ACLTable {
    private static final Logger LOG = LoggerFactory
            .getLogger(ACLTable.class);

    private List<SubnetUtils2> subnets;

    private String name="ACLTable";

    public ACLTable() {
        init("ACLTable");
    }

    public ACLTable(List<SubnetUtils2> subnets) {
        this.subnets = subnets;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 初始化ACL表
     */
    public void init(String name) {
        this.name=name;
        subnets=Collections.synchronizedList(new ArrayList<SubnetUtils2>());
    }

    /**
     * 从文件读取名单
     * @param resource 资源
     */
    public void loadList(Resource resource) {
        try (InputStream is=resource.getInputStream();
            InputStreamReader isr = new InputStreamReader(is, "UTF-8");
            BufferedReader br = new BufferedReader(isr);) {
            String line = "";
            LOG.info("{} Load list file:{}",getName(),resource.getURL());
            while ((line = br.readLine()) != null) {
                LOG.info("{} add {}",getName(),line);
                addSubnet(line);
            }
        } catch (IOException e) {
            LOG.error("load ACL Table error:",e);
        }
    }
    /**
     * 从文件读取名单
     * @param file 文件名
     */
    public void loadList(String file) {
        loadList(new FileSystemResource(file));
    }

    /**
     * 保持列表
     */
    /**
     * 清空ACL表
     */
    public void clear() {
        LOG.debug("清空acl表");
        init(name);
    }

    /**
     * 增加一个子网，如果已经存在则不重复加
     * @param subnetstr 子网
     */
    public void addSubnet(String subnetstr) {
        SubnetUtils2 subnet=new SubnetUtils2(subnetstr);
        if (!isInTable(subnet)) {
            subnets.add(subnet);
        }
    }

    /**
     * 增加一个子网，如果已经存在则不重复加
     * @param subnet 子网
     */
    public void addSubnet(SubnetUtils2 subnet) {
        if (!isInTable(subnet))
            subnets.add(subnet);
    }

    /**
     * 删除一个子网
     * @param subnet 需要删除的子网
     */
    public void removeSubnet(SubnetUtils2 subnet) {
        Iterator<SubnetUtils2> it = subnets.iterator();
        while (it.hasNext()) {
            SubnetUtils2 subnetinset = it.next();
            if (subnetinset.equals(subnet)) {
                it.remove();
                break;
            }
        }
    }

    /**
     * 检查子网是否已经在表里
     * @param subnet 需检查的子网
     * @return true or flase
     */
    public boolean isInTable(SubnetUtils2 subnet) {
        for (SubnetUtils2 oldsubnet:subnets) {
            if (oldsubnet.equals(subnet))
                return true;
        }
        return false;
    }

    /**
     * 检查ip是否已经在表里
     * @param ipaddress 需检查的ip
     * @return 0 不在
     *         !0 在
     */
    public int isInTable(int ipaddress) {
        for (SubnetUtils2 subnet:subnets) {
            if (subnet.getInfo().isInRange(ipaddress))
                return 1;
        }
        return 0;
    }
}
