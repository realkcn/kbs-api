/*************************************************************************
	> File Name: libthrift.c
	> Author:
	> Mail:
	> Created Time: 2014年12月04日 星期四 16时50分57秒
 ************************************************************************/

#include<stdio.h>

#include "gen-c_glib/service_types.h"
#include <thrift/c_glib/transport/thrift_socket.h>
#include <thrift/c_glib/protocol/thrift_protocol.h>
#include <thrift/c_glib/protocol/thrift_binary_protocol.h>
#include <thrift/c_glib/transport/thrift_framed_transport.h>


#ifndef LIB_DACL
#ifdef ENABLE_DYNAMIC_ACL
static ThriftSocket *tsocket = NULL;

ThriftBinaryProtocol *thrift_protocol = NULL;

static int SERVERPORT = 1099;

static ThriftTransport *transport;

GError *thrift_error = NULL;

int thrift_init()
{
    g_type_init();

    tsocket = (ThriftSocket *) g_object_new(THRIFT_TYPE_SOCKET, "hostname", "127.0.0.1", "port", SERVERPORT, NULL);
    transport = THRIFT_TRANSPORT(g_object_new(THRIFT_TYPE_FRAMED_TRANSPORT, "transport", tsocket, 0));
    thrift_protocol = (ThriftBinaryProtocol *) g_object_new(THRIFT_TYPE_BINARY_PROTOCOL, "transport", transport, NULL);
    return thrift_transport_open(transport, 0);
}

void thrift_destroy()
{
    thrift_transport_close(transport, 0);
    g_object_unref(thrift_protocol);
    g_object_unref(transport);
    g_object_unref(tsocket);
}
#endif
#endif
