/*************************************************************************
	> File Name: libdacl.c
	> Author:
	> Mail:
	> Created Time: 2014年12月04日 星期四 16时50分12秒
 ************************************************************************/

#include<stdio.h>

#ifndef LIB_DACL
#ifdef ENABLE_DYNAMIC_ACL
#include "gen-c_glib/dynamic_a_c_l_service.h"
#include "gen-c_glib/service_types.h"
#include <thrift/c_glib/transport/thrift_socket.h>
#include <thrift/c_glib/protocol/thrift_protocol.h>
#include <thrift/c_glib/protocol/thrift_binary_protocol.h>
extern GError *thrift_error;

extern ThriftBinaryProtocol *thrift_protocol;

extern ThriftSocket *thrift_init();

extern void thrift_destroy();

#define SERVER_INIT \
    if (thrift_init()==FALSE) ret=-2;  \
	DynamicACLServiceClient *client = (DynamicACLServiceClient *) g_object_new( \
	    TYPE_DYNAMIC_A_C_L_SERVICE_CLIENT, "input_protocol", thrift_protocol,   \
	    "output_protocol", thrift_protocol, NULL);                              \
	DynamicACLServiceIf *iface = DYNAMIC_A_C_L_SERVICE_IF(client);              \
	gint32		_return;

#define SERVER_CALL(method) \
	thrift_error = NULL;  \
	if ( method== TRUE) {         \
		if (thrift_error == NULL)  \
			ret = _return;         \
		else                       \
			g_error_free(thrift_error);        \
	};
/**
 * 密码验证错误后调用此函数新增错误记录
 * @param char *id: ID
 * @param unsigned long ip: IP
 * @return  0: success
 * @return <0: failure
 **/
int dynamic_acl_add_record(char *id, unsigned long ip)
{
    int ret = -1;

    SERVER_INIT SERVER_CALL(dynamic_a_c_l_service_if_dynamic_acl_add_record(iface, &_return, id, ip, &thrift_error))
        thrift_destroy();
    return ret;
}

/**
 * 检查IP是否被封禁
 * @param   unsigned long ip: IP地址
 * @return   0: 正常IP未被封禁
 * @return  >0: 已被封禁, 返回的值为距离解除封禁的时长(s)
 * @return  <0: dynamic ACL的黑名单
 **/
long dynamic_acl_check_ip(unsigned long ip)
{
    int ret = 0;

    SERVER_INIT if (ret == -2)
        return 0;
    SERVER_CALL(dynamic_a_c_l_service_if_dynamic_acl_check_ip(iface, &_return, ip, &thrift_error))
        thrift_destroy();
    return ret;
}

int dynamic_acl_clear()
{
    int ret = -1;

    SERVER_INIT if (ret == -2)
        return -1;
    SERVER_CALL(dynamic_a_c_l_service_if_dynamic_acl_clear(iface, &_return, &thrift_error))
        thrift_destroy();
    return ret;
}
#endif
#endif
