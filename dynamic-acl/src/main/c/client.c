/*************************************************************************
	> File Name: client.c
	> Author:
	> Mail:
	> Created Time: 2014年12月04日 星期四 17时27分48秒
 ************************************************************************/

#include <stdio.h>
#include <arpa/inet.h>
extern int dynamic_acl_add_record(char *id, unsigned long ip);

extern long dynamic_acl_check_ip(unsigned long ip);

extern int dynamic_acl_clear();


unsigned long dynamic_acl_ip2long(const char *ip)
{
    unsigned long int addr;

    int len = sizeof(ip);

    if (len == 0 || (addr = inet_addr(ip)) == INADDR_NONE) {
        if (len == sizeof("255.255.255.255") - 1 && !memcmp(ip, "255.255.255.255", sizeof("255.255.255.255") - 1))
            return 0xFFFFFFFF;
        return 0;

    }
    return ntohl(addr);

}

main()
{
    int i;

    for (i = 0; i < 60; i++)
        dynamic_acl_add_record("kcn", dynamic_acl_ip2long("192.168.1.1"));
    for (i = 0; i < 20; i++)
        dynamic_acl_add_record("kxn", dynamic_acl_ip2long("192.168.2.1"));
    for (i = 0; i < 10; i++)
        dynamic_acl_add_record("kin", dynamic_acl_ip2long("192.168.3.1"));

    printf("ban:%d\n", dynamic_acl_check_ip(dynamic_acl_ip2long("192.168.1.1")));

    dynamic_acl_clear();
    printf("ban:%d\n", dynamic_acl_check_ip(dynamic_acl_ip2long("192.168.1.1")));
    for (i = 0; i < 60; i++)
        dynamic_acl_add_record("kcn", dynamic_acl_ip2long("192.168.1.1"));
    for (i = 0; i < 20; i++)
        dynamic_acl_add_record("kxn", dynamic_acl_ip2long("192.168.2.1"));
    for (i = 0; i < 10; i++)
        dynamic_acl_add_record("kin", dynamic_acl_ip2long("192.168.3.1"));
}
