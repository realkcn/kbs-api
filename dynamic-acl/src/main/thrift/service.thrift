namespace java org.kbs.api.dynamicacl.thrift
namespace cpp kbs

service DynamicACLService {
  i32 dynamic_acl_add_record(1:string id,2:i32 ip)
  i32 dynamic_acl_clear()
  i32 dynamic_acl_check_ip(1:i32 ip)
}