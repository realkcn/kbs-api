package org.kbs.api.dynamicacl;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.core.io.ClassPathResource;

import static org.easymock.EasyMock.expect;
import static org.junit.Assert.*;
import static org.kbs.api.dynamicacl.IpAddressUtils.ipToInt;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

/**
 * DynamicACLImpl Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>十二月 3, 2014</pre>
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({System.class,DynamicACLImpl.class,AgedACLTableMapImpl.class})
public class DynamicACLImplTest {

    DynamicACLImpl service;

    AgedACLTableMapImpl aclBlockTable;

    ACLTable aclWhiteTable;

    ACLTable aclBlackTable;
    private ACLTable aclGrayTable;

    static String[] clients={
            "192.168.1.1",
            "192.168.2.1",
            "192.168.3.1",
            "192.168.4.1",
            "192.168.5.1",
            "192.168.6.1",
            "192.168.7.1"
    };

    static int[] clientint=new int[clients.length];

    @BeforeClass
    static public void beforeClass() {
        for (int i=0;i<clients.length;i++)
            clientint[i]=ipToInt(clients[i]);
    }

    @Before
    public void before() throws Exception {
        service=new DynamicACLImpl();

        aclBlockTable= AgedACLTableMapImpl.createAgedACLTable("Block Table", 2 * 60 * 60 * 24 * 1000);

        aclWhiteTable=new ACLTable();
        aclWhiteTable.setName("White Table");

        aclBlackTable=new ACLTable();
        aclBlackTable.setName("Black Table");

        aclGrayTable=new ACLTable();
        aclGrayTable.setName("Gray Table");

        service.setAclBlackTable(aclBlackTable);
        service.setAclWhiteTable(aclWhiteTable);
        service.setAclBlockTable(aclBlockTable);
        service.setAclGrayTable(aclGrayTable);
    }

    @After
    public void after() throws Exception {
    }

    static public void mockSystemCurrentTimeMillis(long returnValue) {
        mockStatic(System.class);
        expect(System.currentTimeMillis()).andReturn(new Long(returnValue)).anyTimes();
        replayAll();
    }
    /**
     * Method: doExpire()
     */
    @Test
    public void testBlackWhiteTable() throws Exception {
        aclWhiteTable.loadList(new ClassPathResource("whitetable.txt"));
        aclBlackTable.addSubnet("192.168.1.3/24");
        assertTrue(service.dynamic_acl_check_ip(clientint[0]) < 0);
        assertTrue(service.dynamic_acl_check_ip(clientint[1]) == 0);
        assertTrue(service.dynamic_acl_check_ip(clientint[2]) == 0);
        assertTrue(service.dynamic_acl_check_ip(clientint[6]) == 0);

        aclBlackTable.addSubnet("192.168.2.3/24");
        aclWhiteTable.addSubnet("192.168.2.3/24");
        assertTrue(service.dynamic_acl_check_ip(clientint[0]) <  0);
        assertTrue(service.dynamic_acl_check_ip(clientint[1]) == 0);
        assertTrue(service.dynamic_acl_check_ip(clientint[2]) == 0);

        long baseTimeLine=10000;
        mockSystemCurrentTimeMillis(baseTimeLine);
        aclBlockTable.addSubnet("192.168.2.3/24",60*1000);
        aclBlockTable.addSubnet("192.168.3.3/24",60*1000);

        verifyAll();
        long timegone=35000;
        mockSystemCurrentTimeMillis(baseTimeLine+timegone);
        assertTrue(service.dynamic_acl_check_ip(clientint[0]) <  0);
        assertTrue(service.dynamic_acl_check_ip(clientint[1]) == 0);
        assertEquals(service.dynamic_acl_check_ip(clientint[2]), 60 - timegone / 1000);
    }

    @Test
    public void testDynamicBlock() throws Exception {
    /* 如果一天内累计错误100次，封禁半天；若错误次数超过20 或 过去2小时内错误次数超过15，封禁1个小时；1小时超过10次封禁10分钟，白名单内有三倍机会 */
        long baseTimeLine=10000;
        aclGrayTable.loadList(new ClassPathResource("whitetable.txt"));
        mockSystemCurrentTimeMillis(baseTimeLine);
        for (int i=0;i<100;i++)
            service.dynamic_acl_add_record("doit",clientint[3]);
        assertEquals(service.dynamic_acl_check_ip(clientint[3]), 12 * 60 * 60);

        for (int i=0;i<23;i++)
            service.dynamic_acl_add_record("doit",clientint[4]);
        assertEquals(service.dynamic_acl_check_ip(clientint[4]),1*60*60);

        for (int i=0;i<2;i++)
            service.dynamic_acl_add_record("doit",clientint[5]);

        //5分钟 +5
        verifyAll();
        baseTimeLine+=5*60*1000;
        mockSystemCurrentTimeMillis(baseTimeLine);
        for (int i=0;i<8;i++)
            service.dynamic_acl_add_record("doit",clientint[5]);
        assertEquals(service.dynamic_acl_check_ip(clientint[5]),10*60);

        //16分钟 +11
        verifyAll();
        baseTimeLine+=11*60*1000;
        mockSystemCurrentTimeMillis(baseTimeLine);
        service.doExpire(); //处理超时
        assertEquals(service.dynamic_acl_check_ip(clientint[5]),0);
        for (int i=0;i<2;i++)
            service.dynamic_acl_add_record("doit",clientint[5]);
        assertEquals(service.dynamic_acl_check_ip(clientint[5]),10*60);
        //一天过16分钟
        verifyAll();
        baseTimeLine+=1 * 24 * 60 * 60 * 1000;
        mockSystemCurrentTimeMillis(baseTimeLine);
        service.doExpire(); //处理超时
        for (int i=3;i<=5;i++)
            assertEquals(service.dynamic_acl_check_ip(clientint[i]),0);
        assertEquals(service.getLogCount(),2+8+2+23+100);

        //二天过3分钟
        verifyAll();
        baseTimeLine+=1 * 24 * 60 * 60 * 1000-16*60*1000+3*60*1000;
        mockSystemCurrentTimeMillis(baseTimeLine);
        service.doExpire(); //处理超时
        for (int i=3;i<=5;i++)
            assertEquals(service.dynamic_acl_check_ip(clientint[i]),0);
        assertEquals(service.getLogCount(),2+8);

        //白名单三倍机会
        for (int i=0;i<10;i++)
            service.dynamic_acl_add_record("doit",clientint[6]);
        assertEquals(service.dynamic_acl_check_ip(clientint[6]),0);
        for (int i=0;i<20;i++)
            service.dynamic_acl_add_record("doit",clientint[6]);
        assertEquals(service.dynamic_acl_check_ip(clientint[6]),10*60);
    }
}
