package org.kbs.api.dynamicacl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.easymock.EasyMock.expect;
import static org.junit.Assert.*;
import static org.kbs.api.dynamicacl.IpAddressUtils.ipToInt;
import static org.powermock.api.easymock.PowerMock.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({System.class,AgedACLTableMapImpl.class})
public class AgedACLTableTest {

    static public void mockSystemCurrentTimeMillis(long returnValue) {
        mockStatic(System.class);
        expect(System.currentTimeMillis()).andReturn(new Long(returnValue)).anyTimes();
        replayAll();
    }

    @Test
    public void testAdd() throws Exception {
        long baseTime=1000;
        mockSystemCurrentTimeMillis(baseTime);
        AgedACLTableMapImpl agedACLTable= AgedACLTableMapImpl.createAgedACLTable("Test ACL Table", 2 * 1000);
        agedACLTable.addSubnet("192.168.2.2/24",40*1000);
        agedACLTable.addSubnet("192.168.3.2/24", 0);

        verifyAll();
//        System.out.printf("%x\n",ipToInt("192.168.2.3"));
        mockSystemCurrentTimeMillis(baseTime + 1999);

        assertEquals(agedACLTable.isInTable(ipToInt("192.168.2.3")),38);
        assertEquals(agedACLTable.isInTable(ipToInt("192.168.3.3")),1);

        agedACLTable.addSubnet("192.168.2.2/24",80*1000);
        verifyAll();
        mockSystemCurrentTimeMillis(baseTime + 2 * 1000);
        agedACLTable.doExpire();
        int expiredTime=agedACLTable.isInTable(ipToInt("192.168.2.3"));
//        assertTrue(expiredTime>0);
        assertEquals(expiredTime,(80*1000+1999-2*1000)/1000);

        assertEquals(agedACLTable.isInTable(ipToInt("192.168.3.3")),0);

        verifyAll();
        mockSystemCurrentTimeMillis(baseTime + 100 * 1000);
        agedACLTable.doExpire();
        assertEquals(agedACLTable.isInTable(ipToInt("192.168.2.3")),0);
        verifyAll();
    }
}